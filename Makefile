
all: gen

# genSessionInfo:
# 	rm -rf gen/sessioninfo
# 	openapi-generator generate \
# 		-i doc/sessioninfo.yaml \
# 		-o gen/sessioninfo \
# 		-g haskell-http-client

# desktopapi:
# 	rm -rf generatedDesktopApi
# 	openapi3-code-generator-exe ../draco/doc/desktop.yaml --output-dir="generatedDesktopApi" --module-name="DesktopAPI" --convert-to-camel-case=true --package-name="desktopapi"

gen:
	mkdir -p json
	yq -S 'del(.CarSetup) | del(.CameraInfo) | del(.RadioInfo) | del(.QualifyResultsInfo) | del(.SessionInfo)' doc/sessions/dallarap217.yaml > json/dallarap217.json

	docker run -it -v \
		`pwd`:/local migamake/json-autotype \
		/local/json/dallarap217.json \
		-o /local/src/SessionInfoModel.hs \
		--no-test \
		-t SessionInfoModel
