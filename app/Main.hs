{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Main where

import           Control.Monad                  ( forM_ )
import qualified Data.Aeson                    as Aeson
import qualified Data.Aeson.Encode.Pretty      as Aeson
import qualified Data.Bifunctor                as BiFunctor
import           Data.Binary
import           Data.Binary.Get
import qualified Data.ByteString               as B
import qualified Data.ByteString.Lazy          as BS
import qualified Data.HashMap.Lazy             as HM
import           Data.Int
import qualified Data.List                     as List
import qualified Data.Maybe                    as Maybe
import qualified Data.Scientific               as Scientific
import qualified Data.Text                     as Text
import qualified Data.Text.Encoding            as Text
import qualified Data.Text.Lazy                as LazyText
import qualified Data.Text.Lazy.Encoding       as LazyText
-- import qualified Data.Yaml                     as Yaml
import           GHC.Generics
import           Polaris.Analyzer
import           Polaris.JSON
import           Polaris.Polaris

main :: IO ()
main = do
  input <- BS.getContents
  -- input <- BS.readFile
  --   "samples\\dallarap217_spa combined 2021-09-15 14-01-06.ibt"

  -- print $ show (Data.Binary.decode input :: Header1)

  let (header, subheader) = parseHeaders input

  -- print header
  -- print subheader

  let varHeaders          = getVarHeaders header input
  -- print varHeaders

  -- let buffer = BS.drop (fromIntegral $ bufferOffset header) input
  -- print $ extractInfo (recordCount subheader)
  --                     (recordCount subheader)
  --                     buffer
  --                     (bufferLength header)
  --                     (toHeadersMap varHeaders)

  print $ analyze header subheader input


  let varHeader = head varHeaders
  print $ getSamples (fromIntegral $ bufferOffset header)
                     (fromIntegral $ bufferLength header)
                     (varType varHeader)
                     (fromIntegral $ offset varHeader)
                     input

  -- let varHeadersBuffer    = getVarHeadersBuffer header input
  -- BS.writeFile "output/varheaders.bin" varHeadersBuffer

  -- let buffers             = getSampleBuffers header input

  -- print $ List.length buffers

  -- let varHeaders = getVarHeadersFromBuffer varHeadersBuffer
  -- print varHeaders

  -- let lapTimes = getLapTimes header varHeaders input

  -- print $ List.map (BiFunctor.second formatLap) (HM.toList lapTimes)

  -- let
  --   one =
  --     BS.take (144 * 3) $ BS.drop (fromIntegral $ varHeaderOffset header) input
  -- print $ runGet getInt32be one

  -- let yaml = Text.encodeUtf8 $ LazyText.toStrict $ LazyText.decodeLatin1
  --       (getSessionInfo header input)
  -- print $ B.take 100 yaml
  -- print yaml

  -- let y =
  --       Yaml.decodeEither' yaml :: (Either Yaml.ParseException SessionInfoModel)

  -- y <- Yaml.decodeFileEither "doc/sessions/dallarap217.yaml" :: IO (Either Yaml.ParseException SessionInfoModel)

  -- print y

  return ()

  -- print $ getSessionInfoModel $ yaml

  -- varHeadersInput <- BS.readFile "test.varheaders.bin"

  -- let headers = getVarHeadersFromBuffer varHeadersBuffer
  -- let maybeLap = List.find (\varHeader -> (LazyText.decodeUtf8 $ name varHeader) == (LazyText.pack "Lap")) headers
  -- print lap

  -- BS.writeFile "output/lap-3.bin" $ oneLapBuffer headers buffers 3

  -- forM_ List.map ( \i -> oneLapBuffer headers buffers i ) [0 .. lapCount subheader] ( \i -> BS.writeFile ("output/lap" ++ (show $ BS.length i) ++".bin") i )

  -- print headers

  -- let headers = getVarHeaders header input
  -- print headers

  --putStrLn $ Text.unpack $ Text.decodeUtf8 $ BS.toStrict $ Aeson.encodePretty $ getJsonAllSamples header headers input


-- oneLapBuffer :: [VarHeader] -> [BS.ByteString] -> Integer -> BS.ByteString
-- oneLapBuffer headers buffers lapNumber = case lap of
--   Just header -> BS.concat $ List.filter
--     (\buffer -> getJsonSingleVarHeaderSample header buffer == value)
--     buffers
--   Nothing -> BS.empty

--  where
--   value = (Text.pack "Lap", Aeson.Number $ Scientific.scientific lapNumber 0)
--   lap   = List.find
--     (\varHeader ->
--       (LazyText.decodeUtf8 $ name varHeader) == (LazyText.pack "Lap")
--     )
--     headers
