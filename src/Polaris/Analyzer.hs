{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
module Polaris.Analyzer where

import           Data.Binary                    ( Word32 )
import           Data.Binary.Get                ( getWord32be
                                                , getWord32le
                                                , runGet
                                                )
import           Data.ByteString.Lazy          as BS
import           Data.HashMap.Lazy              ( (!) )
import qualified Data.HashMap.Lazy             as HM
import qualified Data.List                     as List
import           Polaris.Constants
import           Polaris.Parsing
import           Polaris.Polaris

type Stint = Int
type Lap = Int
type BufferStart = Int
type BufferEnd = Int
type SessionState = Int
type PlayerTrackSurface = Int
type LapTime = Float
type CurrentLapTime = LapTime
type LastLapTime = LapTime
type Index = Int
type Info
  = ( Index
    , Lap
    , [Flag]
    , Polaris.Constants.SessionState
    , TrkLoc
    , PlayerTrackSurface
    , CurrentLapTime
    , LastLapTime
    )
-- type LapMeta = (Stint, Lap, BufferStart, BufferEnd, LapTime)


data LapMeta = LapMeta
  { lapMetaStint       :: Int
  , lapMetaLap         :: Int
  , lapMetaBufferStart :: Int
  , lapMetaBufferEnd   :: Int
  , lapMetaLapTime     :: Float
  }
  deriving Show

analyze :: Header -> SubHeader -> ByteString -> [LapMeta]
analyze header@Header { bufferOffset, bufferLength } SubHeader { recordCount } input
  = List.filter isNotOutlier $ analyze' initialLapMeta current rest
 where
  headers    = getVarHeaders header input
  headersMap = toHeadersMap headers
  buffer     = BS.drop (fromIntegral bufferOffset) input
  extractedInfo =
    extractInfo recordCount recordCount buffer bufferLength headersMap

  current : rest                   = extractedInfo
  (_, lap, _, _, _, _, lapTime, _) = current

  initialLapMeta                   = LapMeta 0 lap 0 0 lapTime
  isNotOutlier LapMeta { lapMetaBufferStart = s, lapMetaBufferEnd = e, lapMetaLapTime = lapTime }
    = s /= e && lapTime > 0

toHeadersMap :: [VarHeader] -> HM.HashMap ByteString VarHeader
toHeadersMap = HM.fromList . List.map (\h -> (name h, h))

analyze' :: LapMeta -> Info -> [Info] -> [LapMeta]
analyze' LapMeta { lapMetaStint, lapMetaBufferStart } (index, lap, _, _, _, _, lapTime, _) []
  = [LapMeta lapMetaStint lap lapMetaBufferStart index lapTime]
analyze' lapMeta current (next : is)
  | isSame    = analyze' lapMeta next is
  | otherwise = lapMetaToStore : analyze' nextLapMeta next is

 where
  isSameLap'     = isSameLap current next
  isSameStint'   = isSameStint current next
  isSame         = isSameLap' && isSameStint'

  (index    , lap    , _, _, _, _, currentLapTime, _          ) = current
  (nextIndex, nextLap, _, _, _, _, _             , lastLapTime) = next

  currentStint   = lapMetaStint lapMeta
  bufferStart    = lapMetaBufferStart lapMeta

  lapTime        = max lastLapTime currentLapTime
  nextStint      = if isSameStint' then currentStint else currentStint + 1

  lapMetaToStore = LapMeta currentStint lap bufferStart index lapTime
  nextLapMeta    = LapMeta nextStint nextLap nextIndex 0 0

isSameLap :: Info -> Info -> Bool
isSameLap c@(_, lap, _, _, sessionState, _, _, _) n@(_, nextLap, _, _, nextSessionState, _, _, _)
  | lap /= nextLap
  = False
  | not $ isSameStint c n
  = False
  | otherwise
  = True

isSameStint :: Info -> Info -> Bool
isSameStint (_, lap, _, _, t, surf, _, _) (_, nextLap, _, _, nextT, nextSurf, _, _)
  | lap > 0 && nextLap == 0
  = False
  | otherwise
  = True

extractInfo
  :: Int
  -> Int
  -> ByteString
  -> Int
  -> HM.HashMap ByteString VarHeader
  -> [Info]
extractInfo recordsCount remaining buffer bufferLength headersMap
  | remaining == 0
  = []
  | otherwise
  = ( recordsCount - remaining
    , lap
    , flags
    , ss
    , surf
    , playerTrackSurface
    , lapCurrentLapTime
    , lapLastLapTime
    )
    : extractInfo recordsCount
                  (remaining - 1)
                  remainingBuffer
                  bufferLength
                  headersMap

 where
  lapHeader                = headersMap ! "Lap"
  lapCurrentLapTimeHeader  = headersMap ! "LapCurrentLapTime"
  lapLastLapTimeHeader     = headersMap ! "LapLastLapTime"
  sessionStateHeader       = headersMap ! "SessionState" -- irsdk_SessionState
  playerTrackSurfaceHeader = headersMap ! "PlayerTrackSurface" -- irsdk_TrkLoc
  sessionFlagsHeader       = headersMap ! "SessionFlags"

  (currentBuffer, remainingBuffer) =
    BS.splitAt (fromIntegral bufferLength) buffer

  getInt             = getIntVar currentBuffer
  getFloat           = getFloatVar currentBuffer

  lap                = getInt lapHeader
  sessionState       = getInt sessionStateHeader
  playerTrackSurface = getInt playerTrackSurfaceHeader
  sessionFlags       = getWord32 currentBuffer sessionFlagsHeader

  lapCurrentLapTime  = getFloat lapCurrentLapTimeHeader
  lapLastLapTime     = getFloat lapLastLapTimeHeader

  flags              = Polaris.Constants.flags sessionFlags
  ss                 = Polaris.Constants.sessionState sessionState
  surf               = trkLoc playerTrackSurface

getIntVar :: ByteString -> VarHeader -> Int
getIntVar buffer VarHeader { offset } =
  runGet getInt $ BS.drop (fromIntegral offset) buffer

getFloatVar :: ByteString -> VarHeader -> Float
getFloatVar buffer VarHeader { offset } =
  runGet getFloat $ BS.drop (fromIntegral offset) buffer

getWord32 :: ByteString -> VarHeader -> Word32
getWord32 buffer VarHeader { offset } =
  runGet getWord32le $ BS.drop (fromIntegral offset) buffer
