
module Polaris.Constants where

import           Data.Binary                    ( Word32 )
import           Data.Bits                      ( (.&.) )
import qualified Data.List                     as List
import qualified Data.Tuple                    as Tuple

data Flag
    = Checkered
    | White
    | Green
    | Yellow
    | Red
    | Blue
    | Debris
    | Crossed
    | YellowWaving
    | OneLapToGreen
    | GreenHeld
    | TenToGo
    | FiveToGo
    | RandomWaving
    | Caution
    | CautionWaving

    -- Driver black flags
    | Black
    | Disqualify
    | Servicible -- Car is allowed to service (not a flag)
    | Furled
    | Repair

    -- Start lights
    | StartHidden
    | StartReady
    | StartSet
    | StartGo

    deriving (Show)

flags' :: [(Flag, Word32)]
flags' =
    [ (Checkered    , 0x00000001)
    , (White        , 0x00000002)
    , (Green        , 0x00000004)
    , (Yellow       , 0x00000008)
    , (Red          , 0x00000010)
    , (Blue         , 0x00000020)
    , (Debris       , 0x00000040)
    , (Crossed      , 0x00000080)
    , (YellowWaving , 0x00000100)
    , (OneLapToGreen, 0x00000200)
    , (GreenHeld    , 0x00000400)
    , (TenToGo      , 0x00000800)
    , (FiveToGo     , 0x00001000)
    , (RandomWaving , 0x00002000)
    , (Caution      , 0x00004000)
    , (CautionWaving, 0x00008000)
    , (Black        , 0x00010000)
    , (Disqualify   , 0x00020000)
    , (Servicible   , 0x00040000)
    , (Furled       , 0x00080000)
    , (Repair       , 0x00100000)
    , (StartHidden  , 0x10000000)
    , (StartReady   , 0x20000000)
    , (StartSet     , 0x40000000)
    , (StartGo      , 0x80000000)
    ]

data TrkLoc
    = NotInWord
    | OffTrack
    | InPitStall
    | ApproachingPits
    | OnTrack
    deriving (Show, Eq)

data SessionState
    = StateInvalid
    | StateGetInCar
    | StateWarmup
    | StateParadeLaps
    | StateRacing
    | StateCheckered
    | StateCoolDown
    deriving (Show, Eq)

sessionState :: Int -> SessionState
sessionState 0 = StateInvalid
sessionState 1 = StateGetInCar
sessionState 2 = StateWarmup
sessionState 3 = StateParadeLaps
sessionState 4 = StateRacing
sessionState 5 = StateCheckered
sessionState _ = StateCoolDown

trkLoc :: Int -> TrkLoc
trkLoc (-1) = NotInWord
trkLoc 0    = OffTrack
trkLoc 1    = InPitStall
trkLoc 2    = ApproachingPits
trkLoc _    = OnTrack

flags :: Word32 -> [Flag]
flags flag = map fst $ filter (\i -> snd i .&. flag == snd i) flags'
