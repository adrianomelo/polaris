{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Polaris.JSON where

import           Data.Aeson
import           Data.Binary
import           Data.Binary.Get
import           Data.ByteString.Lazy          as BS
import           Data.HashMap.Lazy             as HM
import           Data.Int
import           Data.List                     as List
import           Data.Scientific
import           Data.Text                     as Text
import           Data.Text.Encoding            as Text
import qualified Data.Tuple                    as Tuple
import           Polaris.Parsing               as Parsing
import           Polaris.Polaris


getSamples :: Int64 -> Int64 -> Int -> Int64 -> ByteString -> [Value]
getSamples bufferOffset bufferLength varType varOffset content = getSamples'
  varOffset
  varType
  bufferLength
  buffer
  where buffer = BS.drop bufferOffset content

getSamples' :: Int64 -> Int -> Int64 -> ByteString -> [Value]
getSamples' offset varType length buffer
  | isEmpty   = []
  | otherwise = value : getSamples' offset varType length remaining

 where
  isEmpty   = BS.null buffer
  start     = BS.drop offset buffer
  value     = getValue varType start
  remaining = BS.drop length buffer

getSampleObjectList :: Header -> [VarHeader] -> ByteString -> [Value]
getSampleObjectList Header { bufferOffset, bufferLength } headers content =
  List.map (getVars headers content) offsets

 where
  offsets =
    List.takeWhile
        (\offset -> not . BS.null $ BS.drop (fromIntegral offset) content)
      $ List.iterate (+ bufferLength) bufferOffset

getLapTimes :: Header -> [VarHeader] -> BS.ByteString -> HashMap Value Value
getLapTimes Header { bufferOffset, bufferLength } headers input =
  case (lapHeader, lapTimeHeader) of
    (Just lap, Just time) -> createMap lap time
    _                     -> HM.empty

 where
  lapHeader     = List.find (isKey "Lap") headers
  lapTimeHeader = List.find (isKey "LapCurrentLapTime") headers
  offsetsInf    = List.iterate (+ bufferLength) bufferOffset
  isNotEmpty offset = not . BS.null $ BS.drop (fromIntegral offset) input
  offsets = List.map fromIntegral $ List.takeWhile isNotEmpty offsetsInf
  chunk bs offset = BS.drop offset bs
  justValue header offset = Tuple.snd $ getVar header $ BS.drop offset input
  createMap lap time = HM.fromList $ List.map
    (\offset -> (justValue lap offset, justValue time offset))
    offsets

getVars :: [VarHeader] -> ByteString -> Int -> Value
getVars headers content offset = Object samplesObject
 where
  start         = BS.drop (fromIntegral offset) content
  samplesList   = List.map (`getVar` start) headers
  samplesObject = fromList samplesList


getVar :: VarHeader -> BS.ByteString -> (Text, Value)
getVar VarHeader { varType, offset, name } buffer =
  (Text.decodeUtf8 $ BS.toStrict name, value)

 where
  start = BS.drop (fromIntegral offset) buffer
  value = getValue varType start


getValue :: Int -> ByteString -> Value
getValue varType buffer
  | varType == 0 = String $ runGetChar buffer
  | varType == 1 = Bool $ runGetBool buffer
  | varType == 2 = Number $ scientific (runGetInteger buffer) 0
  | varType == 3 = Number $ scientific (runGetInteger buffer) 0
  | varType == 4 = Number $ fromFloatDigits $ runGetFloat buffer
  | varType == 5 = Number $ fromFloatDigits $ runGetDouble buffer
  | otherwise    = Null


formatLap :: Value -> Text
formatLap (Number n) = time
 where
  number      = toRealFloat n :: Double
  seconds     = floor number `mod` 60
  minutes     = floor (number / 60)
  miliseconds = floor $ significand number * 1000
  list        = List.map (Text.pack . show) [minutes, seconds, miliseconds]
  time        = Text.intercalate ":" list
formatLap _ = ""


isKey :: Text -> VarHeader -> Bool
isKey key varHeader = Text.decodeUtf8 (BS.toStrict $ name varHeader) == key

