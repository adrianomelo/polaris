module Polaris.Parsing where
import           Data.Binary
import           Data.Binary.Get
import           Data.Binary.IEEE754
import           Data.ByteString.Lazy          as BS
import           Data.Int
import           Data.Text                     as Text
import           Prelude


getInt :: Get Int
getInt = fromIntegral <$> getInt32le

runGetInt :: ByteString -> Int
runGetInt = runGet getInt

runGetInteger :: ByteString -> Integer
runGetInteger = fromIntegral . runGetInt


getFloat64 :: Get Float
getFloat64 = realToFrac <$> getFloat64le -- Still not correct

runGetFloat64 :: ByteString -> Float
runGetFloat64 = runGet getFloat64


getFloat :: Get Float
getFloat = realToFrac <$> getFloatle -- Still not correct

runGetFloat :: ByteString -> Float
runGetFloat = runGet getFloat


getDouble :: Get Double
getDouble = getDoublele

runGetDouble :: ByteString -> Double
runGetDouble = runGet getDouble


getString :: Int64 -> Get ByteString
getString = fmap (BS.takeWhile (/= 0)) . getLazyByteString


getChar :: Get Text
getChar = Text.pack . show <$> getWord8

runGetChar :: ByteString -> Text
runGetChar = runGet Polaris.Parsing.getChar


runGetBool :: ByteString -> Bool
runGetBool = runGet get
