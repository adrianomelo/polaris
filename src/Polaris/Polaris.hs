{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE NamedFieldPuns  #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}

module Polaris.Polaris where

import qualified Data.Aeson                    as Aeson
import           Data.Aeson                     ( FromJSON
                                                , ToJSON
                                                )
import           Data.Aeson.Encode.Pretty       ( NumberFormat(Scientific) )
import           Data.Binary                    ( Binary(get)
                                                , decode
                                                , get
                                                , getWord8
                                                )
import           Data.Binary.Get                ( Get
                                                , getDoublele
                                                , getFloatle
                                                , getInt32be
                                                , getInt32le
                                                , getInt8
                                                , getLazyByteString
                                                , getWord64le
                                                , runGet
                                                , skip
                                                )
import           Data.Binary.IEEE754            ( getFloat64le )
import           Data.Bits                      ( )
import           Data.ByteString.Lazy          as BS
                                                ( ByteString
                                                , drop
                                                , length
                                                , null
                                                , splitAt
                                                , take
                                                , takeWhile
                                                , toStrict
                                                , unpack
                                                )
import qualified Data.HashMap.Lazy             as HM
import           Data.Int                       ( Int
                                                , Int32
                                                , Int64
                                                )
import           Data.List                     as List
                                                ( find
                                                , foldl1
                                                , iterate
                                                , map
                                                , take
                                                , takeWhile
                                                )
import qualified Data.Scientific               as Scientific
import           Data.Text                     as Text
                                                ( Text
                                                , intercalate
                                                , pack
                                                )
import           Data.Text.Encoding            as Text
                                                ( decodeUtf8 )
import qualified Data.Tuple                    as Tuple
import           Data.Word                      ( Word32
                                                , Word64
                                                , Word8
                                                )
import           Data.Yaml                      ( ParseException )
import           Data.Yaml.Aeson                ( decodeEither' )
import           GHC.Exts                       ( fromList )
import           GHC.Generics                   ( Generic )
import           GHC.Int                        ( Int8 )
import qualified GHC.Real                      as Scientific
import           Polaris.Parsing


-- https://github.com/vipoo/irsdk/blob/master/irsdk_defines.h#L302
data Header = Header
    { version           :: Int
    , status            :: Int
    , tickRate          :: Int
    , sessionInfoUpdate :: Int
    , sessionInfoLength :: Int
    , sessionInfoOffset :: Int
    , numberOfVars      :: Int
    , varHeaderOffset   :: Int
    , bufferNumber      :: Int
    , bufferLength      :: Int
    , bufferOffset      :: Int
    }
    deriving (Show, Generic)

instance Binary Header where
    get = do
        version           <- getInt
        status            <- getInt
        tickRate          <- getInt
        sessionInfoUpdate <- getInt
        sessionInfoLength <- getInt
        sessionInfoOffset <- getInt
        numberOfVars      <- getInt
        varHeaderOffset   <- getInt
        bufferNumber      <- getInt
        bufferLength      <- getInt
        _                 <- getInt32le
        _                 <- getInt32le
        _                 <- getInt32le
        bufferOffset      <- getInt

        do
            return $ Header version
                            status
                            tickRate
                            sessionInfoUpdate
                            sessionInfoLength
                            sessionInfoOffset
                            numberOfVars
                            varHeaderOffset
                            bufferNumber
                            bufferLength
                            bufferOffset


data SubHeader = SubHeader
    { startDate   :: Float
    , startTime   :: Double
    , endTime     :: Double
    , lapCount    :: Int
    , recordCount :: Int
    }
    deriving (Show, Generic)

instance Binary SubHeader where
    get = do
        startDate   <- getFloat64
        startTime   <- getDouble
        endTime     <- getDouble
        lapCount    <- getInt
        recordCount <- getInt

        do
            return $ SubHeader startDate startTime endTime lapCount recordCount


-- https://github.com/vipoo/irsdk/blob/master/irsdk_defines.h#L270
data VarHeader = VarHeader
    { varType     :: Int
    , offset      :: Int
    , count       :: Int
    , countAsTime :: Int8
    , name        :: BS.ByteString
    , description :: BS.ByteString
    , unit        :: BS.ByteString
    }
    deriving (Generic, Show)

instance FromJSON BS.ByteString => FromJSON VarHeader
instance ToJSON BS.ByteString => ToJSON VarHeader

instance Binary VarHeader where
    get = do
        inputVarType <- getInt
        offset       <- getInt
        count        <- getInt
        countAsTime  <- getInt8
        _            <- skip 3
        name         <- getString 32
        description  <- getString 64
        unit         <- getString 32

        do
            return $ VarHeader inputVarType
                               offset
                               count
                               countAsTime
                               name
                               description
                               unit


-- data Sample = Sample
--     { sample :: BS.ByteString
--     , value  :: VarType
--     }
--     deriving (Generic, Show)

-- data VarType
--     = CharType Char
--     | BoolType Bool
--     | IntType Int
--     | BitFieldType Int
--     | FloatType Float
--     | DoubleType Double
--     | ETCountType
--     deriving (Generic, Show)

-- instance Binary VarType


parseHeaders :: BS.ByteString -> (Header, SubHeader)
parseHeaders content = (decode header, decode subHeader)

  where
    (header, headerRest) = BS.splitAt 112 content
    subHeader            = BS.take 32 headerRest


getSessionInfo :: Header -> BS.ByteString -> BS.ByteString
getSessionInfo Header { sessionInfoLength, sessionInfoOffset } =
    BS.take (fromIntegral sessionInfoLength)
        . BS.drop (fromIntegral sessionInfoOffset)


getSessionInfoModel :: (FromJSON a) => BS.ByteString -> Either ParseException a
getSessionInfoModel = decodeEither' . toStrict


getVarHeaders :: Header -> BS.ByteString -> [VarHeader]
getVarHeaders header = getVarHeadersFromBuffer . getVarHeadersBuffer header


getVarHeadersBuffer :: Header -> BS.ByteString -> BS.ByteString
getVarHeadersBuffer Header { varHeaderOffset, numberOfVars } =
    BS.take (fromIntegral numberOfVars * 144)
        . BS.drop (fromIntegral varHeaderOffset)


getVarHeadersFromBuffer :: BS.ByteString -> [VarHeader]
getVarHeadersFromBuffer buffer
    | isEmpty   = []
    | otherwise = decode buffer : getVarHeadersFromBuffer remaining

  where
    isEmpty   = BS.null buffer
    remaining = BS.drop 144 buffer


getSampleBuffers :: Header -> BS.ByteString -> [BS.ByteString]
getSampleBuffers Header { bufferOffset, bufferLength } content =
    getSampleBuffers' bufferLength buffer
    where buffer = BS.drop (fromIntegral bufferOffset) content

getSampleBuffers' :: Int -> BS.ByteString -> [BS.ByteString]
getSampleBuffers' length buffer
    | isEmpty
    = []
    | otherwise
    = BS.take sampleLength buffer : getSampleBuffers' length remaining

  where
    sampleLength = fromIntegral length
    isEmpty      = BS.null buffer
    remaining    = BS.drop sampleLength buffer
